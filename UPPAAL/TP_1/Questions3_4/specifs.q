//This file was generated from UPPAAL 3.4.5, Mar 2004

/*
Absence de blocage
*/
A[] not deadlock

/*
Pb de synchro
*/
E<> gauche.before_a and droit.before_b

/*
Pb de synchro
*/
E<> gauche.before_b and droit.before_a


/*
Possibilite d'avoir des communications sur a (gauche -> droite)
*/
E<> gauche.before_a



/*
Possibilite d'avoir des communications sur b (droite -> gauche)
*/
E<> gauche.before_b


/*
Possibilite d'avoir infiniment des communications sur a (gauche -> droite)
*/
E[] not gauche.before_b

/*
Possibilite d'avoir infiniment des communications sur b (droite -> gauche)
*/
E[] not gauche.before_a


