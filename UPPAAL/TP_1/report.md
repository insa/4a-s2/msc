# TP 1 : Problème du Choix Distant
## Question 1 : Choix résolu par la synchronisation
Graphe non-bloquant, inévitablement réinitialisable, monotone, vivant

1. **Exhibez une exécution où la communication a lieu de gauche à droite**
```uppaal
(idle, idle)
s: gauche -> droit
(choice, choice)
a: gauche -> droit
(after_a, after_a)
gauche
(reinit, after_a)
droit
(reinit, reinit)
f: droit -> gauche
```

2. **Exhibez une exécution où la communication a lieu de droite à gauche**
```uppaal
(idle, idle)
s: gauche -> droit
(choice, choice)
b: droit -> gauche
(after_b, after_b)
gauche
(reinit, after_b)
droit
(reinit, reinit)
f: droit -> gauche
```

3. **Persuadez-vous qu'il existe une exécution infinie comportant uniquement des communications de gauche à droite**
Après l'exécution de la séquence **1.**, le système retourne à son état initial `(idle, idle)` donc il peut de nouveau réaliser la même séquence `gauche à droite`. Le système n'a pas de mémoire donc l'exécution infinie a une probabilité très faible d'arriver, mais non nulle.

4. **Persuadez-vous qu'il existe une exécution infinie comportant uniquement des communications de droite à gauche**
Même réponse que **3.** mais avec la séquence de **2.**

5. **Persuadez-vous que le système ne bloque pas**
Lorsque le signal `s` est émis par `gauche`, il est forcément reçu par `droit`. Ils sont nécessairement dans le même état `choice`.
Ensuite, la communication est synchrone : soit `a` est émis par `gauche`, soit `b` est émis par `droit` et donc ils vont évoluer en même temps vers l'état `after_[a|b]` pour ensuite se réinitialiser chacun de leur côté (`reinit`). Enfin, le signal `f` émis par `droit` permet au système de se resynchroniser, et donc le système ne peut pas bloquer.

Si on fait le graphe de tous les états ateignables, on voit qu'on peut faire une action à partir de chaque état.

|                      | [] *pout tous états* | <> *il existe état* |
|:--------------------:|:--------------------:|:--------------------:
| *pour tout chemin* A |       A[] *phi*      |      A<> *phi*      |
| *il existe chemin* E |       E[] *phi*      |      E<> *phi*      |

## Question 2 : Choix non déterministe
### 1 - Edition
1. **Dessinez les automates associés à `gauche` et à `droite`**

## 2 - Simulation
1. **Exhibez une exécution où la communication a lieu de gauche à droite**
```uppaal
(idle, idle)
s: gauche -> droit
(choice, choice)
droit
(choice, before_a)
gauche
(before_a, before_a)
a: gauche -> droit
(after_a, after_a)
f: droit -> gauche
(idle, idle)
```

2. **Exhibez une exécution où la communication a lieu de droite à gauche**
```uppaal
(idle, idle)
s: gauche -> droit
(choice, choice)
gauche
(before_a, choice)
droit
(before_b, choice)
droit
(before_b, before_b)
b: droit -> gauche
(after_b, after_b)
f: droit -> gauche
```

3. **Exhibez une exécution où le système est bloqué**
```uppaal
(idle, idle)
s: gauche -> droit
(choice, choice)
droit
(choice, before_b)
gauche
(before_a, before_b)
```

En utilisant le vérifieur, on peut entrer la formule `E<> deadlock`.

## 3 - Choix autoritaire
1. **Implantez cette solution, vous utiliserez la synchronisation sur le canal `s` et une variable partagée pour transmettre le choix du maître à l'esclave**
2. **Simulez le système et persuadez-vous que tous les scénarios décrits dans la section 1 sont valides**
3. **Pour vérifier - et non plus se persuader - que votre système est correct, allez sous le vérificateur. Chargez (`menu query`) le fichier de propriétés `specifs.q`. Assurez-vous que chaque propriété est bien satisfaite.**
Toutes les requêtes sont vérifiées.
4. **Reprenez la solution précédente en utilisant un seul processus `agent` paramétré qui vous permettra - après instantiation - d'obtenir une instance du processus `left` et une instance du processus `right`.
J'ai utilisé un paramètre `int is_master` qui permets à l'instantiation de spécifier le comportement des deux agents, avec des `guard is_master == [0|1]`.

## 4 - Solution à autorité tournante
Il suffit dans la transition `idle -> choice` de rajouter l'affectation `assign is_master := [0|1]`.
