# TP 2 : Problème du Choix Distant en Asynchrone
## 1 - File avec et sans contrôle de flux
1. **Simulez le système `choixasync.xta`**
2. **Vérifiez les propriétés décrites dans le fichier `spec1.q`.
Toutes les propriétés sont vérifiées avec le fichier initial.
3. **En quoi le processus `fifo` assure-t-il un contrôle de flux ?**
Ce processus a un buffer `buf` et possèce un compteur `cpt` qui permets de savoir combien d'octets sont dans le tube. Ce processus d'assure qu'il ne soit pas possible d'engorger le tube : `put` permets de récupérer un octet ssi `cpt < cap`.
4. **Modifiez le processus `fifo` de telle façon qu'il puisse - suivant son instantiation - assurer un contrôle de flux (même comportement que précédemment) ou non ; dans ce cas vous ferez en sorte que tout ajoût dans une file ayant atteint sa capacité la met dans un état d'erreur.**
J'ai ajouté un paramètre `int controle_flux` lors de l'instantiation d'un FIFO. Ensuite, j'ai rajoute un arc avec la condition `guard controle_flux == 1 && cpt >= cap` de `service` vers `erreur`.
5. **Simulez le système avec le processus `fifo` ainsi modifié en considérant les deux cas : avec et sans contrôle de flux. Vérifiez dans les deux cas les propriétés décrites dans le fichier `spec1.q`.**
Avec l'insertion de la capacité d'un `fifo` d'aller en erreur, toutes les propriétés restent vérifiées sauf la première `A[] not deadlock`. Ainsi, il est possible d'aller dans l'état erreur et le contrôle de flux permets de s'en rendre compte.
6. **Expliquez l'état de blocage constaté sur la version sans contrôle de flux. Donnez une formule permettant de le caractériser.**
7. **En quoi serait-il illusoire de vouloir augmenter la capacité des files de communication ?**
Ca ne sert à rien d'augmenter la capacité : ça ne ferait que repousser le problèm à plus tard sans le résoudre, et donc le problème se poserait nécessairement plus tard.

## 2 - Contrôle de flux explicite entre les sites

