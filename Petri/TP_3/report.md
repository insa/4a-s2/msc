Cette représentation du token ring est mauvaise pour deux raisons :
* Le jeton peut ignorer un client en *Ask* : **invariant de transition** (cycle sans passer par le client)
* Un client peut monopoliser la ressource (refaire un cycle avant de laisser partir le jeton) : **invariant de transition**

Chaque client est indépendant : les invariants de classe prouvent l'exclusion mutuelle entre les clients donc il ne peut y avoir qu'une seule section critique à la fois *(sans avoir besoin de faire un observateur)*
