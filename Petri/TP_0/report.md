[Home page](http://homepages.laas.fr/francois/POLYS/)
[mfoughal@laas.fr](mailto:mfoughal@laas.fr)

# Modélisation de Systèmes Concurrents

* **Système concurrent :** machines qui ont des processus parallèles ; modélisation possible grâce à des sémaphores *(en C, C++)*
* **Réseaux de Petri :** permets de faire de l'analyse grâce à un formalisme automatisable. L'exemple ne permets pas de faire de preuve alors qu'on peut démontrer des propriétés rigoureuses

Invention : 1962-1966 *Communication with automata*

Réseau bloquant : il existe un marquage accessible à partir du marquage initial tel qu'on ne peut plus tirer de transition

**Composantes Fortement Connexes :** Permets d'analyser les réseaux pour connaître la vivacité du réseau

Pour savoir si le réseau est bloquant, condition nécessaire et suffisante : il existe une CFC telle que :
* On ne peut pas quitter la CEC : elle est **pendante**
* Réduite à un point
* N'a pas de boucle *(un point et sans boucle : **triviale**)*
